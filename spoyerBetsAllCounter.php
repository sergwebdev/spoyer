<?php
$domain = 'spoyer.com';

$login = 'hxddxnhvnd';

$token = '81729-UB45Gi9sNFeLT19';


$sport = isset($sport) ? $sport : 'soccer';

$day = isset($day) ? $day : 'today';

$jsonPatch = MODX_BASE_PATH . 'assets/libs/spoyer/' . $sport.'-'.$day.'.json';

if (file_exists($jsonPatch)) {
    
    $json = file_get_contents($jsonPatch);

    $respArr = json_decode($json, true);

    
} else {
    
    $curlUrl = 'https://'.$domain.'/api/get.php?login='.$login.'&token='.$token.'&task=predata&sport='.$sport.'&day='.$day;
    
    $curl = curl_init();

    curl_setopt_array($curl, array(
    	CURLOPT_URL => $curlUrl,
    	CURLOPT_RETURNTRANSFER => true,
    	CURLOPT_FOLLOWLOCATION => true,
    	CURLOPT_ENCODING => "",
    	CURLOPT_MAXREDIRS => 10,
    	CURLOPT_TIMEOUT => 30,
    	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    	CURLOPT_CUSTOMREQUEST => "GET",
    ));
    
    $resp = curl_exec($curl);
    			
    curl_close($curl);
    
    $respArr = json_decode($resp, true);
    
}

/* Отсеять Esports с результатов */
foreach ($respArr['games_pre'] as $k => $v) {

    $leagueName = $v['league']['name'];
    
    if (substr($leagueName, 0, 7) == 'Esoccer') {
        
        unset($respArr['games_pre'][$k]);
        
    }
    
    
}

if(!isset($respArr)) {
    $out = '0';
} else {
    $out = count($respArr['games_pre']);
}

return $out;
