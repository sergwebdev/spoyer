<?php
if(isset($league) && $league != '') {

    $result = $modx->query("SELECT * FROM leagues WHERE league_id = $league");
    
    if (is_object($result)) {
       
       $row = $result->fetch(PDO::FETCH_ASSOC);
       
       if($row['cc'] != '') {
           
           $cc = $row['cc'];
           
           $out .= '<img title="'.$cc.'" class="matches__inner__liga__icon" width="16" height="16" src="https://spoyer.ru/api/icons/countries/'.$row['cc'].'.svg" alt="'.$cc.'" loading="lazy">';
           
       }

       $out .= '<span class="matches__inner__liga__name">'.$row['league_name'].'</span>';

    } 
 
} else {
    $out = '<span class="matches__inner__liga__name">'.$modx->resource->get('pagetitle').'</span>';
}

return $out;
