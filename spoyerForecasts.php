
<?php
$domain = 'spoyer.com';

$login = 'hxddxnhvnd';

$token = '81729-UB45Gi9sNFeLT19';

$pdoTools = $modx->getService('pdoTools');

$tpl = isset($tpl) ? $tpl : 'tplSpoyerForecast';

$sport = isset($sport) ? $sport : 'soccer';

$day = isset($day) ? $day : 'today';

if(isset($league) && $league != '') {
    
    $curlUrl = 'https://'.$domain.'/api/get.php?login='.$login.'&token='.$token.'&task=predatapage&sport='.$sport.'&league='.$league.'&p=1';
    
} else {
    
    $curlUrl = 'https://'.$domain.'/api/get.php?login='.$login.'&token='.$token.'&task=predatapage&sport='.$sport.'&day='.$day.'&p=1';
    
}

$curl = curl_init();

curl_setopt_array($curl, array(
	CURLOPT_URL => $curlUrl,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
));

$resp = curl_exec($curl);
			
curl_close($curl);
			
$respArr = json_decode($resp, true);

//echo '<pre>';
//print_r($respArr);
//echo '</pre>';

foreach ($respArr['games_pre'] as $k => $v) {
    

    
    $leagueName = $v['league']['name'];
    
    $leagueId = $v['league']['id'];

    //$leagueArr[$leagueId] = $leagueName;

    $gameId = $v['game_id'];

    $gameStartDay = date('d.m', $v['time']);
    
    $gameStartTime = date('H:i', $v['time']);
                    
    $gameDateTime = date('Y-m-dTH:i:s', $v['time']);
    

    $teamHomeName = $v['home']['name'];
    
    $teamHomeId = $v['home']['id'];
    
    $teamHomeImgUrl = 'https://spoyer.ru/api/team_img/'.$sport.'/'.$teamHomeId.'.png';
    
    $teamAwayName = $v['away']['name'];
    
    $teamAwayId = $v['away']['id'];
    
    $teamAwayImgUrl = 'https://spoyer.ru/api/team_img/'.$sport.'/'.$teamAwayId.'.png';
    
    if (exif_imagetype($teamHomeImgUrl) != IMAGETYPE_PNG) {
           $teamHomeImgUrl = '/app/images/new__design/icons/forecast__empty__logo.svg';
    }
    
    if (exif_imagetype($teamAwayImgUrl) != IMAGETYPE_PNG) {
           $teamAwayImgUrl = '/app/images/new__design/icons/forecast__empty__logo.svg';
    }

    
    $placeholders = array(
        
        'gameId' => $gameId,    
        'leagueName' => $leagueName,
        'teamHomeName' => $teamHomeName,
        'teamAwayName' => $teamAwayName,
        'teamHomeImgUrl' => $teamHomeImgUrl,
        'teamAwayImgUrl' => $teamAwayImgUrl,
        'gameStartDay' => $gameStartDay,
        'gameStartTime' => $gameStartTime,
        'gameDateTime' => $gameDateTime,
                            

    );
                    
    //$out .= $pdoTools->getChunk('@FILE '.$tpl, $placeholders);
                    
    $out .= $pdoTools->getChunk($tpl, $placeholders);
    

}

//$uniqLeagues = array_unique($leagueArr);





return $out;
